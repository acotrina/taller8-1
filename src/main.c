#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "../include/utilfecha.h"

int main(int argc, char **argv){
	char opcion;
	while( (opcion = getopt(argc, argv, "h:a:f:")) != -1){
		switch(opcion){
			case 'h':
				parseSegToHoras(atoi(optarg));
				break;
			case 'a':
				parseDiasToAnios(atoi(optarg));
				break;
			case 'f':
				parseToFecha(optarg);
				break;
		}
	}

	return 0;
}

