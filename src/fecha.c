#define _XOPEN_SOURCE //variable necesaria de acuerdo al manual de strptime
#include <stdio.h>
#include <stdlib.h> 
#include <time.h>
#include "../include/utilfecha.h"

char* getMonth(int n){
	char *month=NULL;
	switch(n){
        case 1:month="Enero";
            break;
        case 2:month="Febrero";
            break;
        case 3:month="Marzo";
            break;
        case 4:month="Abril";
            break;
        case 5:month="Mayo";
            break;
        case 6:month="Junio";
            break;
        case 7:month="Julio";
            break;
        case 8:month="Agosto";
            break;
        case 9:month="Septiembre";
            break;
        case 10:month="Octubre";
            break;
        case 11:month="Noviembre";
            break;
        case 12: month="Diciembre";
            break;
    }
    return month;
}

void parseToFecha(char* fecha){
	struct tm parsedDate;
	int year=0, month=0, day=0;

	if(strptime(fecha, "%Y-%m-%d", &parsedDate)== NULL){
		printf("Fecha incorrecta\n");
	}else{
		year=parsedDate.tm_year+1900;
		month=parsedDate.tm_mon+1;
		day=parsedDate.tm_mday;

        printf("%d de %s de %d\n",day,getMonth(month),year);
   }

}
