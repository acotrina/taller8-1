#include <stdio.h>
#include "../include/utilfecha.h"

void parseDiasToAnios(int days){
	int  anios=0, meses=0, dias=0;
	anios= days/ANIO;
	meses= ((days/MES)%MES)%NRO_MESES;
	dias=days%MES;
	printf("años\tmeses\tdias\n");
	printf("%d\t%d\t%d\n",anios,meses,dias);
}