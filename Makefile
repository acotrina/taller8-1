CC=gcc
FLAGS=-Wall -c -O3
all:bin/dinamico bin/estatico



bin/estatico: obj/main_estatico.o lib/libdateutils.a
	
	gcc -static -Wall -L -l obj/main_estatico.o ./lib/libdateutils.a -o bin/estatico

lib/libdateutils.a:obj/segundos_estatico.o obj/dias_estatico.o obj/fecha_estatico.o
	ar rcs lib/libdateutils.a obj/segundos_estatico.o obj/dias_estatico.o obj/fecha_estatico.o

bin/dinamico:obj/main_dinamico.o lib/libdateutils.so
	gcc -Wall -L -l obj/main_dinamico.o ./lib/libdateutils.so -o bin/dinamico

lib/libdateutils.so:
	gcc -shared -fPIC -I include/ src/dias.c src/main.c src/segundos.c src/fecha.c

obj/main_estatico.o: src/main.c 
	$(CC)  src/main.c -o $@

obj/segundos_estatico.o: src/segundos.c include/utilfecha.h
	$(CC) $(FLAGS)  src/segundos.c -L include/ -o $@

obj/dias_estatico.o: src/dias.c include/utilfecha.h
	$(CC) $(FLAGS) src/dias.c -L include/ -o $@

obj/fecha_estatico.o: src/fecha.c include/utilfecha.h
	$(CC) $(FLAGS) src/fecha.c -L include/ -o $@


obj/main_dinamico.o: src/main.c 
	$(CC) $(FLAGS) -fPIC src/main.c -o $@

obj/segundos_dinamico.o: src/segundos.c include/utilfecha.h
	$(CC) $(FLAGS) -fPIC src/segundos.c -L include/ -o $@

obj/dias_dinamico.o: src/dias.c include/utilfecha.h
	$(CC) $(FLAGS) -fPIC src/dias.c -L include/ -o $@

obj/fecha_dinamico.o: src/fecha.c include/utilfecha.h
	$(CC) $(FLAGS) -fPIC src/fecha.c -L include/ -o $@




.PHONY:clean
clean:
	rm -rf obj bin
	mkdir obj bin
	
