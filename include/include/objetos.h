//En este archivo ud podra definir los objetos que podrán ser usados 
//con el slab allocator. Declare tambien el constructor y destructor de 
//cada objeto declarado. Abajo se provee un objeto de ejemplo.
//Declare al menos tres objetos distintos

//El constructor siempre recibe un puntero tipo void al tipo de objeto,
//y su tamaño (tal como se declaro el puntero a funcion).
//No olvide implementar las funciones de crear y destruir de cada objeto.

typedef struct ejemplo{
    int a;
    float b[100];
    char *msg;
    unsigned int refcount;
} Ejemplo;

//Constructor
void crear_Ejemplo(void *ref, size_t tamano);

//Destructor
void destruir_Ejemplo(void *ref, size_t tamano);

//TODO: Crear 3 objetos mas
//Objeto Programacion 
typedef struct programacion{
	int n;
	char c[60];
	char *mensaje; 
} Programacion;

void crear_Programacion(void *ref, size_t tamano);
void destruir_Programacion(void *ref, size_t tamano);
// Objeto de 
typedef struct de{
	int c;
	long arr[50];
	int *nota;
} De;

void crear_De(void *ref, size_t tamano);
void destruir_De(void *ref, size_t tamano);

typedef struct sistemas{
	char p;
	float c[30];
	char *msg;
} Sistemas;

void crear_Sistemas(void *ref, size_t tamano);
void destruir_Sistemas(void *ref, size_t tamano);
